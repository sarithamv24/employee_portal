# EmPloyee_Portal

To manage the employee details.

1) Add an employee - http://localhost:8080/api/v1/employee

2) To update the salary of an Employee -  http://localhost:8080/api/v1/employee/{id}/{salary}

3) Get the details of employee for a department - http://localhost:8080/api/v1/employee/{deptId}